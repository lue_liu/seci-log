#seci-log
<div>
    <p>
     赛克蓝德日志分析软件，主要对日志进行收集，格式化，然后进行分析，日志可以是系统日志，也可以是业务日志，业务日志需要二次开发。目前支持linux登录相关的安全日志。采集方式，目前支持syslog日志，后续会增加文件，数据库日志的收集。
    </p>
<p>随着互联网和云计算的发展，公有云服务器是人们越来越容易接受的产品，其最普遍受益的一点就是节省成本。企业不必像拥有私有云那样去购买，安装，操作或运维服务器或是其他设备。在一个公有云的服务供应商提供的平台上，企业只需使用或开发他们自己的应用程序即可。但公有云的安全问题也是显而易见的，基于Internet的公有云服务的特性，全世界只要能上网的人就可以访问到其云服务器，其在云主机及其云上的数据受到威胁会更多而且更复杂，数据相对于私有云处于一个不稳定的状态。不管是传统的信息化还是未来趋势的云计算，都面临着安全的风险，从安全防护的角度来说，需要一个循序渐进的方式去完善安全体系。一般建设的顺序是网络安全、主机安全、数据安全的顺序逐步完善。</p>
<p>但对于很多中小企业来说，本身的设备也不是太多，也就几台到几十台而已，如果花费太多的精力去搞安全也不划算，如果不搞又感觉不放心。那什么方面的内容是中小企业关注的呢？个人认为应该优先关注访问安全，就是有没有人非法访问你的服务器，因为在云平台下，任何人只要接入网络都可以访问到你的机器。所以我认为应该优先关注此信息，报告非上班时间访问，非上班地点访问，密码猜测，账号猜测，账号猜测成功等行为。从我了解的情况下，这部分目前还没有比较有效的开源或者免费工具供大家使用，现在elk用的比较多，但大多数情况下都不太适合中小公司，门槛太高。因此本公司针对这种情况，专门开发了赛克蓝德日志分析软件。可以针对上面的几种情况进行分析，部署简单学习成本很低。</p>
<p>首先上一张门户图：</p>
<p><img class="alignnone" src="http://youxiablog.qiniudn.com/wp-content/uploads/2015/04/c600004f4c91209078f3d82ffbbd6902.png?imageView2/2/w/575/h/280" alt="" width="575" height="280"></p>
<p>下面来分析下几种告警：</p>
<h4>非上班时间登录</h4>
<p>本次告警规则为非上班时间登录系统，主要的目的是防止有人在非上班时间登录系统，这种情况是比较危险的。</p>
<p><img class="alignnone" src="http://youxiablog.qiniudn.com/wp-content/uploads/2015/04/30352860bbca467ca6834a6b1a8b511b.png?imageView2/2/w/575/h/34" alt="" width="575" height="34"></p>
<p>验证过程：</p>
<p>首先配置非上班时间，这个系统已经内置，在安全配置的安全配置中。默认0点到8点，晚上20点到24点为非上班时间。</p>
<p><img class="alignnone" src="http://youxiablog.qiniudn.com/wp-content/uploads/2015/04/bae78cb067b705340ba1c9fd887fcdcf.png?imageView2/2/w/575/h/149" alt="" width="575" height="149"></p>
<p>然后再用ssh连接工具，比如SecureCRT登录系统。这个时候就会有一条日志记录。</p>
<p><img class="alignnone" src="http://youxiablog.qiniudn.com/wp-content/uploads/2015/04/db1141c65ea745a4d6d8c4760d896d09.png?imageView2/2/w/576/h/57" alt="" width="576" height="57"></p>
<p>从日志上可以看出，登录时间为21:32:28秒，是属于非上班时间。登录后等个两三分中到WEB关系系统中查看日志和告警。</p>
<p><img class="alignnone" src="http://youxiablog.qiniudn.com/wp-content/uploads/2015/04/4060b636b9ae806bf80d2f249ee7eb25.png?imageView2/2/w/576/h/111" alt="" width="576" height="111"></p>
<p><img class="alignnone" src="http://youxiablog.qiniudn.com/wp-content/uploads/2015/04/9dd38814d1593fafdcb9c4d8c1db1350.png?imageView2/2/w/576/h/126" alt="" width="576" height="126"></p>
<p><img class="alignnone" src="http://youxiablog.qiniudn.com/wp-content/uploads/2015/04/bba87b57e86bbeea3719637fc546868a.png?imageView2/2/w/575/h/147" alt="" width="575" height="147"></p>
<p>可以看出系统记录了日志，并产生了告警。</p>
<h4>非上班地点登录</h4>
<p>本次告警规则为非上班地点登录系统，主要的目的是防止有人在非上班地点登录系统，这种情况是比较危险的。</p>
<p><img class="alignnone" src="http://youxiablog.qiniudn.com/wp-content/uploads/2015/04/f61af35dce18d5b8729bd5db68aeac7c.png?imageView2/2/w/576/h/39" alt="" width="576" height="39"></p>
<p>验证过程：</p>
<p>首先配置上班地点。这些数据要根据工作环境设置。从中可以看出这里里面没有192.168.21.1。需要注意的是配置完成后需要重新启动采集器来加载配置参数。</p>
<p><img class="alignnone" src="http://youxiablog.qiniudn.com/wp-content/uploads/2015/04/120b2e3f8b55fc4764ecbf333897f0d4.png?imageView2/2/w/575/h/149" alt="" width="575" height="149"></p>
<p>验证的过程和非上班时间一致。这个时候会产生一条非上班地点告警。需要注意的是对同一条事件如果满足多个告警规则，会生成多条告警，但日志只有一条。</p>
<p><img class="alignnone" src="http://youxiablog.qiniudn.com/wp-content/uploads/2015/04/774adea19d60aff2e5ea58d448937dc3.png?imageView2/2/w/575/h/158" alt="" width="575" height="158"></p>
<p><img class="alignnone" src="http://youxiablog.qiniudn.com/wp-content/uploads/2015/04/264e3df42003ec722e28fc4c303ae94e.png?imageView2/2/w/575/h/148" alt="" width="575" height="148"></p>
<p>从告警日志的详情来看和刚才的非上班时间登录是同一条日志。</p>
<p>当把192.168.21.1添加到上班地点中的时候，在做一次登录操作。这时候发现并没有产生告警，则表示此规则生效。</p>
<h4>密码猜测攻击</h4>
<p>密码猜测攻击是一种非常常见的攻击手段，其特征是一段时间内容有连续的错误登录日志，则可以确定为密码猜测攻击。</p>
<p><img class="alignnone" src="http://youxiablog.qiniudn.com/wp-content/uploads/2015/04/ad77dae4e279357365c905e899713153.png?imageView2/2/w/575/h/47" alt="" width="575" height="47"></p>
<p>验证过程：</p>
<p>在一段时间内连续输错密码即可。从日志上看，在短时间内输错了3次密码。</p>
<p><img class="alignnone" src="http://youxiablog.qiniudn.com/wp-content/uploads/2015/04/5551a98ccd04ed8f857b37eb855ebef3.png?imageView2/2/w/566/h/160" alt="" width="566" height="160"></p>
<p>产生的告警如下：</p>
<p><img class="alignnone" src="http://youxiablog.qiniudn.com/wp-content/uploads/2015/04/516dccf37bc915112275807c08f0629f.png?imageView2/2/w/575/h/129" alt="" width="575" height="129"></p>
<p>对应的事件：</p>
<p><img class="alignnone" src="http://youxiablog.qiniudn.com/wp-content/uploads/2015/04/516dccf37bc915112275807c08f0629f.png?imageView2/2/w/575/h/129" alt="" width="575" height="129"></p>
<p>这里面有个疑问就是多条日志在这里面只有一条日志，但对应的告警数量是3，这是因为在系统中对原始事件做了归并处理，当系统发现原始事件是一类的时候，就把这些事件合并成一条事件，对应的事件数量为实际发生的数量。</p>
<h4>账号猜测攻击</h4>
<p>账号猜测攻击是一种非常常见的攻击手段，一般被攻击者首先要确定主机的账号后才能对其发起进一步的攻击。所以一般攻击者首先会猜测root的账号，所以修改root账号名称或者禁止root账号远程登陆是非常有效的安全手段。其特征是一段时间内容有连续的账号不存在登录日志，则可以确定为账号猜测攻击。告警规则如下：</p>
<p><img class="alignnone" src="http://youxiablog.qiniudn.com/wp-content/uploads/2015/04/aae8d4f29b11a47b00a5f37adc5347f6.png?imageView2/2/w/575/h/24" alt="" width="575" height="24"></p>
<p>验证过程，用系统中不存在的账号登录系统。</p>
<p><img class="alignnone" src="http://youxiablog.qiniudn.com/wp-content/uploads/2015/04/d3d4bdc7dfada9df1d2abc00a80090b6.png?imageView2/2/w/571/h/264" alt="" width="571" height="264"></p>
<p>产生的告警如下：</p>
<p><img class="alignnone" src="http://youxiablog.qiniudn.com/wp-content/uploads/2015/04/fb44dd7671319f13577f9c8b55f5148f.png?imageView2/2/w/576/h/88" alt="" width="576" height="88"></p>
<p>所对应的日志详情如下：</p>
<p><img class="alignnone" src="http://youxiablog.qiniudn.com/wp-content/uploads/2015/04/19b3bd3daebcd48010fc4e5a81206804.png?imageView2/2/w/576/h/132" alt="" width="576" height="132"></p>
<h4>密码猜测攻击成功</h4>
<p>密码猜测攻击成功是一种非常严重的攻击，表示攻击者已经攻击成功，进入了系统，这种情况是非常危险的，尤其要重点关注此告警。这种告警的主要特点是，刚开始的时候，用户有密码猜测的行为，紧接着用户会有一条登录成功的行为，这两种行为结合起来后就可以得出密码猜测攻击成功。</p>
<p>验证过程，首先用错误的密码连续登录系统几次，之后再用正确的密码登录。</p>
<p><img class="alignnone" src="http://youxiablog.qiniudn.com/wp-content/uploads/2015/04/d0b27cf9a7fd6c8975cf9c4ce9aa1cf9.png?imageView2/2/w/571/h/232" alt="" width="571" height="232"></p>
<p>产生的告警，可以看到产生了两条告警，一条是密码猜测攻击，一条是密码猜测攻击成功。</p>
<p><img class="alignnone" src="http://youxiablog.qiniudn.com/wp-content/uploads/2015/04/de7c44df00970eb5f76fcb9b8f433264.png?imageView2/2/w/575/h/127" alt="" width="575" height="127"></p>
<p>我们在看一下系统里面记录的日志，明显能看到，先有5此失败登录，紧接着有一条成功的日志。</p>
<p><img class="alignnone" src="http://youxiablog.qiniudn.com/wp-content/uploads/2015/04/d322694607f3e29d8c6be4c07c77d626.png?imageView2/2/w/576/h/146" alt="" width="576" height="146"></p>
<p>从上面可以看出，基本上可以满足中小公司对登录日志分析的要求。</p>
<p>下载地址：</p>
<p>产品下载：http://pan.baidu.com/s/1qWt7Hxi</p>
<p>欢迎试用，并提宝贵建议。</p>
</div>